import 'package:get_it/get_it.dart';
import 'package:note_app/services/database_service.dart';
import 'package:note_app/services/navigation_service.dart';
import 'package:sqflite_migration_service/sqflite_migration_service.dart';

final locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => DatabaseService());
  locator.registerLazySingleton(() => DatabaseMigrationService());
}