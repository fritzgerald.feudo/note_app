import 'package:note_app/app/locator.dart';
import 'package:note_app/models/note.dart';
import 'package:note_app/routing/route_names.dart';
import 'package:note_app/services/database_service.dart';
import 'package:note_app/services/navigation_service.dart';
import 'package:stacked/stacked.dart';

class HomeViewModel extends FutureViewModel<List<Note>> {
  final _databaseService = locator<DatabaseService>();
  final _navigationService = locator<NavigationService>();

  @override
  Future<List<Note>> futureToRun() => _databaseService.getNotes();

  Future createNote() async => _navigationService.navigateTo(noteRoute);

  Future editNote({required int? id}) async
  => _navigationService.navigateTo(noteRoute, arguments: id);
}