import 'package:flutter/material.dart';
import 'package:note_app/view/home/home_viewmodel.dart';
import 'package:stacked/stacked.dart';

class HomeView extends StatelessWidget {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
      viewModelBuilder: () => HomeViewModel(),
      builder: (context, model, child) => Scaffold(
          appBar: AppBar(
            title: const Text("Notes"),
          ),
          body: model.dataReady && (model.data?.isNotEmpty ?? false) ? ListView.builder(
              itemCount: model.data!.length,
              itemBuilder: (context, index) => Card(
                child: ListTile(
                    title: Text(model.data![index].title),
                    onTap: () async {
                      await model.editNote(id: model.data![index].id);
                    },
                ),
              ),
          ) : Container(),
          floatingActionButton: FloatingActionButton(
            onPressed: () async {
              await model.createNote();
            },
            tooltip: 'Increment',
            child: const Icon(Icons.add),
          ),
        ),
    );
  }
}
