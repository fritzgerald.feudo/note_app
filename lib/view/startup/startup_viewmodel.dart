import 'package:note_app/app/locator.dart';
import 'package:note_app/routing/route_names.dart';
import 'package:note_app/services/database_service.dart';
import 'package:note_app/services/navigation_service.dart';
import 'package:stacked/stacked.dart';

class StartupViewModel extends BaseViewModel {
  final _navigationService = locator<NavigationService>();
  final _databaseService = locator<DatabaseService>();

  Future initialise() async {
    await _databaseService.initialise();
    await _navigationService.navigateTo(homeRoute);
  }
}