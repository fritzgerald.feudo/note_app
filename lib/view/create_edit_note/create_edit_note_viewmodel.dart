import 'package:note_app/app/locator.dart';
import 'package:note_app/models/note.dart';
import 'package:note_app/routing/route_names.dart';
import 'package:note_app/services/database_service.dart';
import 'package:note_app/services/navigation_service.dart';
import 'package:stacked/stacked.dart';

class CreateEditNoteViewModel extends FutureViewModel<Note> {
  final _navigationService = locator<NavigationService>();
  final _databaseService = locator<DatabaseService>();

  int? id;

  CreateEditNoteViewModel({this.id});

  @override
  Future<Note> futureToRun() async
    => await _databaseService.getNote(id: id);

  Future saveNote({String title = "", String content = ""}) async {
    if (id == null) {
      await _databaseService.addNote(title: title, content: content);
    } else {
      await _databaseService.updateNote(id: id!, title: title, content: content);
    }

    _navigationService.navigateTo(homeRoute);
  }

  Future deleteNote() async {
    if (id != null) {
      await _databaseService.deleteNote(id: id!);
    }

    _navigationService.navigateTo(homeRoute);
  }
}