import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:note_app/view/create_edit_note/create_edit_note_viewmodel.dart';
import 'package:stacked/stacked.dart';

class CreateEditNoteView extends HookWidget {

  const CreateEditNoteView({super.key});

  @override
  Widget build(BuildContext context) {
    int? id = ModalRoute.of(context)?.settings.arguments as int?;
    final titleController = useTextEditingController(text: "");
    final contentController = useTextEditingController(text: "");

    return ViewModelBuilder<CreateEditNoteViewModel>.reactive(
        viewModelBuilder: () => CreateEditNoteViewModel(id: id),
        builder: (context, model, child) {
          titleController.text = model.data?.title ?? "";
          contentController.text = model.data?.content ?? "";

          return model.dataReady ? Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: const Icon(Icons.keyboard_arrow_left),
              onPressed: () {
                model.saveNote(
                  title: titleController.text,
                  content: contentController.text,
                );
              },
            ),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: TextField(
                    controller: titleController,
                    decoration: const InputDecoration(
                        hintText: 'Title',
                        hintStyle: TextStyle(color: Colors.grey),
                        border: InputBorder.none,
                    ),
                  ),
                ),
                Visibility(
                  visible: model.id != null,
                  child: IconButton(
                    icon: const Icon(Icons.delete),
                    onPressed: () async {
                      await model.deleteNote();
                    }
                  ),
                ),
              ],
            ),
          ),
          body: Container(
            padding: const EdgeInsets.all(10),
            child: TextField(
              maxLines: 1000,
              controller: contentController,
              decoration: const InputDecoration(
                hintText: 'Enter text...',
                hintStyle: TextStyle(color: Colors.grey),
                border: OutlineInputBorder( // Add border
                  borderSide: BorderSide(color: Colors.blue), // Set border color
                ),
              ),
            ),
          ),
        ) : Container();
      }
    );
  }
}
