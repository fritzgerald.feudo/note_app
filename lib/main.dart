import 'package:flutter/material.dart';
import 'package:note_app/app/locator.dart';
import 'package:note_app/routing/router.dart';
import 'package:note_app/services/navigation_service.dart';
import 'package:note_app/view/startup/startup_view.dart';

void main() {
  setupLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Stacked Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      navigatorKey: locator<NavigationService>().navigatorKey,
      onGenerateRoute: generateRoute,
      home: const StartupView(),
    );
  }
}