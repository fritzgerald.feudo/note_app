import 'package:flutter/material.dart';
import 'package:note_app/routing/route_names.dart';
import 'package:note_app/view/create_edit_note/create_edit_note_view.dart';
import 'package:note_app/view/home/home_view.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  debugPrint('generateRoute: ${settings.name}');
  switch (settings.name) {
    case homeRoute:
      return _getPageRoute(const HomeView());
    case noteRoute:
      return _getPageRoute(const CreateEditNoteView(), arguments: settings.arguments);
    default:
      return _getPageRoute(const HomeView());
  }
}

PageRoute _getPageRoute(Widget child, {Object? arguments}) {
  return MaterialPageRoute(
    builder: (context) => child,
    settings: RouteSettings(
      arguments: arguments,
    ),
  );
}