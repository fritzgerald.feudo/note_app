import 'package:flutter/material.dart';
import 'package:note_app/app/locator.dart';
import 'package:note_app/models/note.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_migration_service/sqflite_migration_service.dart';

const String dbName = 'todo_database.sqlite';
const String noteTableName = 'notes';

class DatabaseService {
  final _migrationService = locator<DatabaseMigrationService>();
  late Database _database;

  Future initialise() async {
    _database = await openDatabase(dbName, version: 1);

    // Apply migration on every start
    await _migrationService.runMigration(
      _database,
      migrationFiles: [
        '1_create_schema.sql',
        '2_seed_notes.sql',
      ],
      verbose: true,
    );
  }

  Future<List<Note>> getNotes() async {
    final noteResults = await _database.query(noteTableName);
    return noteResults.map((e) => Note.fromJson(e)).toList();
  }

  Future<Note> getNote({int? id}) async {
    try {
      final noteResult = (await _database.query(
        noteTableName,
        where: 'id = ?',
        whereArgs: [id],
        limit: 1,
      )).firstOrNull;

      if (noteResult != null) {
        return Note.fromJson(noteResult);
      }
    } catch (e) {
      debugPrint('Could not get the note: $e');
    }

    return Note();
  }

  Future addNote({String title = "", String content = ""}) async {
    try {
      await _database.insert(
        noteTableName,
        Note(
          title: title,
          content: content,
        ).toJson(),
      );
    } catch (e) {
      debugPrint('Could not insert the note: $e');
    }
  }

  Future updateNote({required int id, String title = "", String content = ""}) async {
    try {
      await _database.update(
        noteTableName,
        {
          'title': title,
          'content': content,
        },
        where: 'id = ?',
        whereArgs: [id],
      );
    } catch (e) {
      debugPrint('Could not update the note: $e');
    }
  }

  Future deleteNote({required int id}) async {
    try {
      await _database.delete(
        noteTableName,
        where: 'id = ?',
        whereArgs: [id],
      );
    } catch (e) {
      debugPrint('Could not delete the note: $e');
    }
  }
}